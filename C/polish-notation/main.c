#include <stdio.h>
//#include "stack.h"

int operation (int *stack, int *pivot, char symbol) {
	int val1 = stack[*pivot - 1];
	int val2 = stack[*pivot - 2];
	stack[*pivot - 1] = '\0';
	stack[*pivot - 2] = '\0';

	*pivot -= 2;

	if (symbol == '+')
		stack[*pivot] = val2 + val1;
	else if (symbol == '-')
		stack[*pivot] = val2 - val1;
	else if (symbol == '*')
		stack[*pivot] = val2 * val1;
	else if (symbol == '/')
		stack[*pivot] = val2 / val1;

	*pivot += 1;

	return 1;
}

int main (int argc, char *argv[]) {

	char chars[1000] = "2 3 11 + 5 -*";
	int intStack[1000] = {};
	int intStackPivot = 0;
	int tmpNumber;
	int prevSymb = 0;

	for (int x = 0; chars[x] != '\0'; x++) {
		if (chars[x] == ' ') {
			if (prevSymb)
				continue;

			printf("%d\n", tmpNumber);
			intStack[intStackPivot] = tmpNumber;
			intStackPivot++;
			tmpNumber = 0;
		}
		else if (chars[x] == '+' || chars[x] == '-'
			|| chars[x] == '*' || chars[x] == '/') 
			prevSymb = operation(&intStack[0], &intStackPivot, chars[x]);
		else {
			prevSymb = 0;
			tmpNumber *= 10;
			tmpNumber += chars[x] - 48;
		}
	}

	if (intStack[1] != '\0') {
		printf("Error!!!>> Refactorize the expresion\n");
		return 1;
	}else {

		printf("Result = %d\n", intStack[0]);
		return 0;
	}

}
